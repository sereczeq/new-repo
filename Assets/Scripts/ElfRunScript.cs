﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElfRunScript : MonoBehaviour
{
    public bool run;
    public bool goBack;
    
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        animator.SetTrigger("RunActual");
    }

    // Update is called once per frame
    void Update()
    {

        if (goBack)
        {
            animator.SetTrigger("GoBack");
            goBack = false;
        }
        
    }
}
