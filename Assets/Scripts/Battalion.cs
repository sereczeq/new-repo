﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battalion : MonoBehaviour
{
    public bool isElven = true;

    private GameObject circle;
    private MeshRenderer circleRenderer;
    private BattalionManager battalionManager;
    // Start is called before the first frame update
    void Start()
    {
        battalionManager = GameObject.Find("GameManager").GetComponent<BattalionManager>();
        circle = transform.Find("Circle").gameObject;
        circleRenderer = circle.GetComponent<MeshRenderer>();
        circle.SetActive(false);
    }

    public void ShowRed()
    {
        // circleRenderer.material.color = Color.red;
        circle.SetActive(true);
    }
    public void ShowGreen()
    {
        // circleRenderer.material.color = Color.white;
        circle.SetActive(true);
    }
    public void DontShowCircle()
    {
        circle.SetActive(false);
    }

    public void StartSoldiers()
    {
        foreach (Transform obj in transform)
        {
            if (obj.CompareTag("Soldier") || obj.CompareTag("Elf"))
            {
                
                obj.GetComponentInChildren<Animator>().SetTrigger("RunActual");
                obj.GetComponentInChildren<Animator>().speed = 1;

            }
        }
    }
}
