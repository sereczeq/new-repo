﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitalRuby.LightningBolt;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using Random = UnityEngine.Random;

public class Scene1Manager : MonoBehaviour
{
    public static Scene1Manager instance;

    public Transform ElvenArmy;
    public Transform EnemyArmy;
    public Transform LevelLoader;
    public Transform Light;
    public Tutorial tutorial;
    private Animator anim;
    
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }

        anim = GameObject.Find("Anim").GetComponent<Animator>();

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.enabled = true;
        }
    }


    public void SceneTransition()
    {
        LevelLoader.GetComponent<Animator>().SetTrigger("Start");
    }
    public void StartEnemy()
    {
        var army  = GameObject.FindGameObjectsWithTag("Soldier");
        foreach (var VARIABLE in army)
        {
            VARIABLE.GetComponentInChildren<Animator>().SetTrigger("Run");            
        }
        
    }

    // public void StartEnemyWalk()
    // {
    //     var army  = GameObject.FindGameObjectsWithTag("Soldier");
    //     foreach (var VARIABLE in army)
    //     {
    //         VARIABLE.GetComponent<ShakeSoldier>().StartCoroutine("StartWalk");
    //     }
    // }

    public void MoveFirstRow()
    {
        EnemyArmy.GetComponent<EnemyArmy>().MoveFirstRow();
    }
    public void StartElven()
    {
        foreach (Transform obj in ElvenArmy)
        {
            if (!obj.CompareTag("Elf"))
            {
                foreach (Transform elf in obj.transform)
                {
                    elf.GetComponentInChildren<Animator>().SetTrigger("Guard");
                }
            }
            else
            {
                obj.GetComponentInChildren<Animator>().SetTrigger("Guard");
            }
        }
    }

    public void TriggerLightning()
    {
        Light.GetComponent<Lightning>().Trigger();
    }
    public void TriggerLightnings()
    {
        Light.GetComponent<Lightning>().TriggerMultiple();
    }

    public void ShowCharacter()
    {
        tutorial.ShowCharacter();
    }
    
}
