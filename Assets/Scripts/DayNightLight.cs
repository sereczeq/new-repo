﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class DayNightLight : MonoBehaviour
{
    public float minTime = 0.2f;

    public float maxTime = 1f;

    private Light light;
    public bool enableDefault = false;
    private void Start()
    {
        light = GetComponent<Light>();
        light.enabled = !enableDefault;
        try
        {

            minTime *= Scene2Manager.instance.dayTime;
            maxTime *= Scene2Manager.instance.dayTime;
        }
        catch (Exception e)
        {
        }
    }

    public void TurnOn()
    {
        StartCoroutine(Turn(true));
    }
    public void TurnOff()
    {
        StartCoroutine(Turn(false));
    }

    public void TurnJust(bool on = true)
    {
        StartCoroutine(Turn(on));
    }

    private IEnumerator Turn(bool on = true)
    {
        yield return new WaitForSeconds(Random.Range(minTime, maxTime));
        light.enabled = on;
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
