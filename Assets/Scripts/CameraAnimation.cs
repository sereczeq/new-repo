﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimation : MonoBehaviour
{
    void StartEnemy()
    {
        Scene1Manager.instance.StartEnemy();
    }
    void StartElven()
    {
        Scene1Manager.instance.StartElven();
    }

    void MoveFirstRow()
    {
        Scene1Manager.instance.MoveFirstRow();
    }

    void TriggerLightning()
    {
        Scene1Manager.instance.TriggerLightning();
    }
    void TriggerLightnings()
    {
        Scene1Manager.instance.TriggerLightnings();
    }
}
