﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crystals : MonoBehaviour
{
    private Transform crystals;
    void Start()
    {
        crystals = GameObject.Find("Crystals").transform;
        foreach (Transform VARIABLE in crystals)
        {
            VARIABLE.gameObject.SetActive(false);
        }
    }

    public void ShowOne()
    {
        foreach (Transform VARIABLE in crystals.transform)
        {
            if (VARIABLE.gameObject.activeSelf) continue;
            VARIABLE.gameObject.SetActive(true);
            return;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
