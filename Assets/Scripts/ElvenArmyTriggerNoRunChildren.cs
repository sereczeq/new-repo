﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ElvenArmyTriggerNoRunChildren : MonoBehaviour
{
    public bool noRun = false;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Run", 1f);
    }

    // Update is called once per frame
    void Update()
    {
        if (noRun)
        {
            noRun = false;
            foreach (Transform elf in transform)
            {
                elf.GetComponentInChildren<Animator>().SetTrigger("GoBack");
            }

            this.enabled = false;
        }
    }

    public void Run()
    {
        foreach (Transform elf in transform)
        {
            Debug.Log(elf);
            elf.GetComponentInChildren<Animator>().SetTrigger("RunActual");
        }
    }
}
