﻿using System.Collections;
using System.Collections.Generic;
using DigitalRuby.LightningBolt;
using UnityEngine;

public class Lightning : MonoBehaviour
{
    public float intensity = 2f;
    public float exposure = 1f;
    public float timeScale = 0.015f;
    public Transform lightningObject;
    private LightningBoltScript lightning;
    private Quaternion startingRotation;
    private float baseIntensity;
    

    private Light lightVariable;
    // Start is called before the first frame update
    void Start()
    {
        startingRotation = transform.rotation;
        lightVariable = GetComponent<Light>();
        baseIntensity = lightVariable.intensity;
        lightning = lightningObject.GetComponent<LightningBoltScript>();
    }

    public void Trigger()
    {
        StartCoroutine("Light");
    }

    public void TriggerMultiple()
    {
        int random = Random.Range(2, 6);
        for (int i = 0; i < random; i++)
        {
            float time = 0f;
            if (i != 1) time = Random.Range(0.4f, 0.7f);
            Invoke("Trigger", time);
        }
        
    }
    
    IEnumerator Light()
    {
        lightningObject.localPosition = new Vector3(Random.Range(-8, 8),
            lightningObject.localPosition.y,
            lightningObject.localPosition.z);
        lightning.Trigger();
        lightning.Trigger();
        lightning.Trigger();
        
        Quaternion rotation = transform.rotation;
        rotation.y = Random.rotation.y;
        transform.rotation = rotation;
        
        lightVariable.intensity = baseIntensity + 3f;
        yield return new WaitForSeconds(timeScale);
        
        lightVariable.intensity = baseIntensity;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {TriggerMultiple();}
        
    }
}
