﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

public class BuilderRoad : MonoBehaviour
{
    [Header("Prefabs")] private GameObject prefab;
    private Building buildingScript;
    public Material greenMaterial;
    public Material redMaterial;

    [FormerlySerializedAs("mask")] [Header("ScriptVars")]
    public LayerMask maskMouse;

    public LayerMask maskRoad;

    private GameObject building;
    private new MeshRenderer renderer;
    private new Camera camera;
    private bool canBuild = false;

    private List<Transform> roads = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (!prefab) return;
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        foreach (var road in GameObject.FindGameObjectsWithTag("Road"))
        {
            if (road != building && !roads.Contains(road.transform))
                roads.Add(road.transform);
        }

        RaycastHit hitMouse;
        var ray = camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hitMouse, Mathf.Infinity, maskMouse))
        {
            building.transform.position = hitMouse.point;
            renderer.material = redMaterial;
            canBuild = false;

            float min = Int32.MaxValue;
            foreach (var road in roads)
            {
                var distance = Vector3.Distance(hitMouse.point, road.position);
                if (distance < min)
                {
                    building.transform.LookAt(road);
                    min = distance;
                }
            }


            var vec = building.transform.eulerAngles;
            vec.x = Mathf.Round(vec.x / 90) * 90;
            vec.y = Mathf.Round(vec.y / 90) * 90;
            vec.z = Mathf.Round(vec.z / 90) * 90;
            building.transform.eulerAngles = vec;

            RaycastHit hitBuilding;
            var point = building.transform;
            if (Physics.Raycast(point.position, point.forward, out hitBuilding, Mathf.Infinity, maskRoad))
            {
                var distanceFromRoad = Vector3.Distance(point.position, hitBuilding.point);
                // Debug.Log(distanceFromRoad);
                if (distanceFromRoad < buildingScript.distanceSticky)
                {
                    building.transform.position =
                        hitBuilding.point - buildingScript.point.position + point.position;
                    if (buildingScript.canPlace)
                    {
                        renderer.material = greenMaterial;
                        canBuild = true;
                    }
                }
            }
            else
            {
                // Debug.Log("not hit");
            }
        }

        if (Input.GetMouseButtonDown(0) && canBuild)
        {
            Scene2Manager.instance.TakeMaterials(buildingScript.gold, buildingScript.wood, buildingScript.brick);
            var newBuilding = Instantiate(prefab, building.transform.position, building.transform.rotation);
            Destroy(newBuilding.GetComponent<Rigidbody>());
            newBuilding.GetComponent<BoxCollider>().isTrigger = false;
            newBuilding.transform.parent = GameObject.Find("Buildings").transform;
            newBuilding.GetComponent<Building>().placed = true;
        }
    }

    public void ChangeBuilding(GameObject newPrefab)
    {
        Destroy(building);
        if (newPrefab is null) return;
        roads.Clear();
        prefab = newPrefab;
        building = Instantiate(prefab, Vector3.zero, Quaternion.identity);
        buildingScript = building.GetComponent<Building>();
        renderer = building.GetComponentInChildren<MeshRenderer>();

        try
        {
            var parent = GameObject.Find("Buildings");
            building.transform.parent = parent.transform;
        }
        catch (NullReferenceException)
        {
            GameObject parent = new GameObject("Buildings");
            building.transform.parent = parent.transform;
        }
    }
}