﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    // Start is called before the first frame update
    // private int currentStage = 0;
    public Transform canvas;
    private Animator characterAnimator;
    private RawImage textBubble;
    private GameObject fightButton;
    private bool JustToMakeSureCharacterWontDissapearAgain = false;
    private bool JustToMakeDoubleSureCharacterWontDissapearAgain = false;

    private TextAnimationScript textInTextBubble;

    // private readonly string text = "Commander! Dwarfs have destroyed the city. Let's rebuild it!"; 
    void Start()
    {
        try
        {
            Scene1Manager.instance.tutorial = this;
        }
        catch (NullReferenceException)
        {
            Debug.Log("No manager 1");
        }


        characterAnimator = canvas.Find("Character").GetComponent<Animator>();
        textBubble = characterAnimator.transform.Find("TextBubble").GetComponent<RawImage>();
        textInTextBubble = textBubble.transform.Find("Text").GetComponent<TextAnimationScript>();
        fightButton = characterAnimator.transform.Find("FightButton").gameObject;
        fightButton.SetActive(false);
        ShowCharacter();
        // textInTextBubble.gameObject.GetComponent<Text>().text = text;
        // GetNextStage();
    }

    // Update is called once per frame
    // void Update()
    // {
    //     if (Input.anyKeyDown && !JustToMakeSureCharacterWontDissapearAgain)
    //     {
    //         characterAnimator.SetTrigger("Disappear");
    //         JustToMakeSureCharacterWontDissapearAgain = true;
    //     }
    // }

    public void StartGame()
    {
        Scene2Manager.instance.StartGame();
    }

    public void ShowNotEnoughResources()
    {
        if (JustToMakeDoubleSureCharacterWontDissapearAgain) return;
        JustToMakeSureCharacterWontDissapearAgain = true;
        textInTextBubble.gameObject.GetComponent<Text>().text = "We need more resources";
        fightButton.SetActive(true);
        ShowCharacter();

    }
    

    // private void GetNextStage()
    // {
    //     switch (currentStage)
    //     {
    //         case 1:
    //         case 2:       
    //             SwitchText(textOptions[currentStage ]);
    //             break;
    //         case 3:
    //             characterAnimator.SetTrigger("Disappear");
    //             break;
    //     }
    //     currentStage++;
    // }

    // private void SwitchText(string text)
    // {
    //     textInTextBubble.SwitchText(text);
    // }

    public void ShowCharacter()
    {
        characterAnimator.SetTrigger("Appear");
        
    }
}