﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TwoArmiesAnimation : MonoBehaviour
{
    private void Start()
    {
    }
    
    public void TriggerGuard()
    {
        Scene1Manager.instance.StartElven();
    }

    public void TriggerRun()
    {
        Scene1Manager.instance.StartEnemy();
    }
    
    
    public void TriggerLightning()
    {
        Scene1Manager.instance.TriggerLightning();
    }
    
    public void TriggerLightnings()
    {
        Scene1Manager.instance.TriggerLightnings();
    }
    

    public void ChangeScene()
    {
        SceneManager.LoadScene(1);
    }
}
