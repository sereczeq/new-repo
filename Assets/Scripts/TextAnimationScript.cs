﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextAnimationScript : MonoBehaviour
{
    private Animator animator;

    private string text;

    private Text textComponent;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        textComponent = GetComponent<Text>();
    }

    public void SwitchText(string newText)
    {
        text = newText;
        // NextAnimation();
        StartAnimation();
    }

    private void NextAnimation()
    {
        textComponent.text = text;
        animator.SetTrigger("StartReversed");
    }
    

    void StartAnimation()
    {
        animator.SetTrigger("Start");
    }
}
