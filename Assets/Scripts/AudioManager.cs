﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    private AudioSource audioSource;
    
    [FormerlySerializedAs("backGround")] public AudioClip intro;

    public AudioClip building;
    public AudioClip battlefield;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }

        audioSource = GetComponent<AudioSource>();
        audioSource.clip = intro;
        audioSource.Play();
    }

    public void PlayBuilding()
    {
        audioSource.clip = building;
        audioSource.Play();
    }

    public void PlayBattlefield()
    {
        audioSource.clip = battlefield;
        audioSource.time = 1f;
        audioSource.Play();
    }
    
}
