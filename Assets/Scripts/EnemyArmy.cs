﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArmy : MonoBehaviour
{
    private Animator _animator;

    public Transform firstRow;

    public Transform secondRow;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        // firstRow.gameObject.SetActive(false);
    }

    public void MoveFirstRow()
    {
        // secondRow.gameObject.SetActive(false);
        // firstRow.gameObject.SetActive(true);
    }

    public void StartTransition()
    {
        Scene1Manager.instance.SceneTransition();
    }
    
    public void StartAnimation()
    {
        _animator.SetTrigger("StartPlaying");
        foreach (Transform row in transform)
        {
            foreach (Transform battalion in row)
            {
                foreach (Transform child in battalion)
                {
                    if (child.gameObject.CompareTag("Soldier"))
                    {
                        child.GetComponent<ShakeSoldier>().StartAnimation();
                        child.GetComponentInChildren<Animator>().SetTrigger("run");
                    }
                }
            }
        }
    }
}