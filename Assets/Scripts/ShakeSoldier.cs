﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;


public class ShakeSoldier : MonoBehaviour
{
    public bool autoStart = false;
    private Vector3 moveTo;
    public float range;
    public float speed;
    public float changeRatio;
    private Vector3 initialPosition;
    public bool walk = true;
    private void Start()
    {
        initialPosition = transform.position;
        if(autoStart) StartAnimation();
        if (walk) StartCoroutine("StartWalk");
    }

    public void StartAnimation()
    {
        CancelInvoke("setMoveTo");
        transform.position = initialPosition;
        InvokeRepeating("setMoveTo", 1f, changeRatio);
        
    }

    public IEnumerator StartWalk()
    {
        yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
        GetComponentInChildren<Animator>().SetTrigger("Walk");
        GetComponentInChildren<Animator>().speed = 2;
    }
    

    private void Update()
    {
        var position = transform.position;
        transform.position = Vector3.Lerp(position, position + moveTo, speed * Time.deltaTime);
    }

    private void setMoveTo()
    {
        moveTo = new Vector3(Random.Range(-range, range) * .5f, 0, Random.Range(-range, range));
    }
}