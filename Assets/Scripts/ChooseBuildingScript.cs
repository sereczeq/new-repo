﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseBuildingScript : MonoBehaviour
{
    public GameObject prefab;


    public void OnClick()
    {
        Scene2Manager.instance.ChangeBuilding(prefab);
    }
}
