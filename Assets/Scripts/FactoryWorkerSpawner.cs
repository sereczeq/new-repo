﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoryWorkerSpawner : MonoBehaviour
{
    public GameObject factoryWorker;

    public float frequency = 1.5f;

    private Building building;
    private bool placed;
    private Transform point;
    // Start is called before the first frame update
    void Start()
    {
        point = transform.Find("point");
        // SpawnWorker();
        InvokeRepeating("SpawnWorker", 0, frequency);
        building = gameObject.GetComponent<Building>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SpawnWorker()
    {
        if (!building.placed) return;
        var worker = Instantiate(factoryWorker, point.position, Quaternion.identity);
        // worker.transform.parent = transform;
    }
    
}
