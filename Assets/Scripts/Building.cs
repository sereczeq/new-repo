﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Building : MonoBehaviour
{

    public string parent;

    public Transform point;
    
    public bool canPlace = true;
    public bool placed = false;

    [FormerlySerializedAs("distance")]
    public float distanceSticky = 27;
    
    [Header("Resources")]
    public int gold = 120;
    public int wood = 78;
    public int brick = 237;


    private void Start()
    {
        canPlace = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Building") || other.CompareTag("Road"))
        {
            canPlace = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Building") || other.CompareTag("Road"))
        {
            canPlace = true;
        }
    }
    
    
}
