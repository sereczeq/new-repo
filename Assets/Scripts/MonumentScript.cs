﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MonumentScript : MonoBehaviour
{
    private void Start()
    {
        AudioManager.instance.PlayBuilding();
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(5);
    }

    public void StopDayNightCycle()
    {
        Scene2Manager.instance.StopDayNightCycle();
    }
    
}
