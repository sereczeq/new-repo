﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattalionManager : MonoBehaviour
{
    public float speed = 0.3f;
    public float distance = 10;
    
    private List<Transform> friendlies;
    private List<Transform> enemies;

    private Vector3 attackPoint;
    private bool isAttacking = false;
    private new Camera camera;

    private int thrice = 0;

    public Transform victoryTransform;
    private Animator victoryAnimator;
    
    public List<GameObject> icons;
    // Start is called before the first frame update
    void Start()
    {
        friendlies = new List<Transform>();
        enemies = new List<Transform>();
        camera = Camera.main;
        victoryAnimator = victoryTransform.GetComponentInChildren<Animator>();
        foreach (var VARIABLE in icons)
        {
            VARIABLE.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isAttacking)
        {
            int walking = 0;
            foreach (var friendly in friendlies)
            {
                if (thrice < 3)
                {
                    friendly.GetComponent<Battalion>().StartSoldiers();
                    thrice++;
                }
                var distanceLocal = Vector3.Distance(friendly.position, attackPoint);
                if (distanceLocal > distance)
                {
                    walking++;
                }
                friendly.position = Vector3.MoveTowards(friendly.position, attackPoint, speed * Time.deltaTime);
                friendly.transform.LookAt(attackPoint);
            }

            if (walking == 0)
            {
                victoryAnimator.SetTrigger("Appear");
            }
            return;
        }
        if (Input.GetMouseButtonDown(1))
        {
            StartAttack();
        }
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hitInfo));
            {
                Transform battalion = hitInfo.transform;
                if (battalion.CompareTag("Battalion"))
                {
                    Battalion script = battalion.GetComponent<Battalion>();
                    
                    if (script.isElven)
                    {
                        if (friendlies.Contains(battalion))
                        {
                            friendlies.Remove(battalion);
                            script.DontShowCircle();
                        }
                        else
                        {
                            friendlies.Add(battalion);
                            icons[0].SetActive(true);
                            icons.Remove(icons[0]);
                            script.ShowGreen();
                        }
                    }
                    else
                    {
                        if (enemies.Contains(battalion))
                        {
                            enemies.Remove(battalion);
                            script.DontShowCircle();
                        }
                        else
                        {
                            enemies.Add(battalion);
                            script.ShowRed();
                        }
                    }
                }
            }
        }
    }

    private void StartAttack()
    {
        if (enemies.Count == 0)
        {
            Debug.Log("No enemies marked");
            return;
        }
        isAttacking = true;
        attackPoint = new Vector3();
        foreach (var enemy in enemies)
        {
            attackPoint += enemy.position;
        }

        attackPoint /= enemies.Count;

    }
    
}