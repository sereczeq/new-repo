﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAnimation : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
Invoke("Anim", 0.4f);
    }

    private void Anim()
    {        
        foreach (Transform VARIABLE in transform)
        {
            // Debug.Log(VARIABLE.name);
            VARIABLE.gameObject.GetComponentInChildren<Animator>().SetTrigger("Run");
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
