﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoryWorker : MonoBehaviour
{
    public float speed;
    public int distance;
    private Transform destination;
    private Rigidbody rb;
    void Start()
    {
        destination = GameObject.Find("Castle").transform;
        rb = GetComponent<Rigidbody>();
        transform.Find("Elf").GetComponentInChildren<Animator>().SetTrigger("run");
        transform.Find("Cart (3)").GetComponentInChildren<Animator>().speed=2;
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log(Vector3.Distance(transform.position, destination.position));
        if (Vector3.Distance(transform.position, destination.position) < distance)
        {
            Scene2Manager.instance.GetComponent<Crystals>().ShowOne();
            Destroy(gameObject);
        }
        var position = destination.position;
        var lookAt = new Vector3(position.x, transform.position.y, position.z);
        transform.rotation = Quaternion.LookRotation(lookAt - transform.position, Vector3.up);
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, destination.position, speed);
        // rb.MovePosition(destination.position.normalized * speed * Time.deltaTime);
    }
}
