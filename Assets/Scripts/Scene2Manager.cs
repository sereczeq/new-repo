﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class Scene2Manager : MonoBehaviour
{
    public static Scene2Manager instance;

    private BuilderRoad builder;
    
    public Transform canvas;
    public Transform textsCanvas;
    public float numberChangeSpeed = 0.0003f;
    public Text goldText;
    public Text woodText;
    public Text brickText;
    
    public Tutorial tutorial;

    private List<DayNightLight> lights;
    public float dayTime = 1f;
    private bool night = false;
    public bool mid = true;
    private bool stop = false;
    private Quaternion initialRotation;
    public GameObject mainLightObject;
    private Light mainLightScript;
    public Color dayColor;
    public Color nighColor;
    
    void Start()
    {
        // initialRotation = mainLightObject.transform.rotation;
        if (instance == null)
        {
            instance = this;
        }

        builder = GetComponent<BuilderRoad>();
        tutorial = GetComponent<Tutorial>();
        // factoryBuilderScript = factoryBuilder.GetComponent<Builder>();
        
        
        // goldText = textsCanvas.Find("GoldPanel").Find("GoldValueText").GetComponent<Text>();
        // woodText = textsCanvas.Find("WoodPanel").Find("WoodValueText").GetComponent<Text>();
        // brickText = textsCanvas.Find("BrickPanel").Find("BrickValueText").GetComponent<Text>();
        // canvas.gameObject.SetActive(false);

        lights = new List<DayNightLight>();
        foreach (var VARIABLE in GameObject.FindGameObjectsWithTag("Light"))
        {
            lights.Add(VARIABLE.GetComponent<DayNightLight>());
        }

        mainLightScript = mainLightObject.GetComponent<Light>();
        InvokeRepeating("ToggleDayNight", 0, dayTime);

        

    }
    // Update is called once per frame
    void Update()
    {
        // if (mainLightObject)
        // {
        //     if (!stop) mainLightObject.transform.Rotate(Vector3.up, (360 / dayTime / 4) * Time.deltaTime);
        //     else mainLightObject.transform.rotation = initialRotation;
        // }

        if (!mid) mainLightScript.color = Color.Lerp(mainLightScript.color, night ? nighColor : dayColor, 2 *1/dayTime * Time.deltaTime);
        // if (!mid)
        //     mainLightScript.intensity =
        //         Mathf.Lerp(mainLightScript.intensity, night ? 0.01f : 1, 2 *1/dayTime * Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.A))
        {
            foreach (var VARIABLE in lights)
            {
                VARIABLE.TurnOn();
            }
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            foreach (var VARIABLE in lights)
            {
                VARIABLE.TurnOff();
            }
        }
        if (Int32.Parse(woodText.text) < 10)
        {
            // ChangeBuilding(null);
            foreach (var Exclamation in GameObject.FindGameObjectsWithTag("Exclamation"))
            {
                foreach (Transform VARIABLE in Exclamation.transform)
                {
                    VARIABLE.gameObject.SetActive(true);
                }
            }
            builder.ChangeBuilding(null);
            foreach (var VARIABLE in GameObject.FindGameObjectsWithTag("Building"))
            {
                VARIABLE.GetComponent<Building>().placed = false;
            }
            textsCanvas.GetComponent<BuildingCanvas>().ShowNarratorTwo();
        }

        // if (Input.GetKeyDown(KeyCode.Space))
        // {
        //     foreach (var Exclamation in GameObject.FindGameObjectsWithTag("Exclamation"))
        //     {
        //         foreach (Transform VARIABLE in Exclamation.transform)
        //         {
        //             VARIABLE.gameObject.SetActive(true);
        //         }
        //     }
        // }
    }

    private void ToggleDayNight()
    {
        mid = !mid;
        if (!mid)
        {
            night = !night;
            foreach (var VARIABLE in lights)
            {
                VARIABLE.TurnJust(night);
            }
        }
    }

    public void StopDayNightCycle()
    {
        stop = true;
        CancelInvoke("ToggleDayNight");
        mid = false;
        night = false;
        foreach (var VARIABLE in lights)
        {
            VARIABLE.TurnJust(night);
        }
    }
    
    

    public void StartGame()
    {
    }

    public void TakeMaterials(int gold, int wood, int brick)
    {
        StartCoroutine(changeText(goldText, gold));
        StartCoroutine(changeText(woodText, wood));
        StartCoroutine(changeText(brickText, brick));
    }

    public IEnumerator changeText(Text text, int value)
    {
        int textValue = Int32.Parse(text.text);
        
        for (int i = 0; i < value; i++, textValue--)
        {
            if (textValue < 0) yield break;
            text.text = textValue.ToString();
            // yield return new WaitForSeconds(numberChangeSpeed);
            yield return new WaitForFixedUpdate();
        }
        
    }

    public void ChangeBuilding(GameObject prefab)
    {
        builder.ChangeBuilding(prefab);
    }
    
    
    
}
