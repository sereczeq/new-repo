﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ElfScript : MonoBehaviour
{
    private Rigidbody _rigidbody;
    public bool started = false;
    public Transform point;
    public float speed;
    public float range = 12f;
    public float rangeScatter = 1f;
    public float rotationScatter = 3f;
    public float speedScatter = 1f;
    void Start()
    {
        started = false;
        // range += Random.Range(-rangeScatter, rangeScatter);
        // speed += Random.Range(-speedScatter, speedScatter);
        point = transform.parent.Find("Point");
        transform.LookAt(point);
        transform.Rotate(0, 180, 0);
        _rigidbody = GetComponent<Rigidbody>();
        // range = Convert.ToSingle(Math.Floor(Vector3.Distance(transform.position, point.position)) + range);
        range = Vector3.Distance(transform.position, point.position) + range;
        // transform.Translate(Vector3.forward * Vector3.Distance(transform.position, point.position));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space)) started = true;
        if (!started) return;
        if (Vector3.Distance(transform.position, point.position) < range)
        {
            transform.LookAt(point);
            transform.Rotate(0, 180, 0);
            // float rotation = Random.Range(-rotationScatter, rotationScatter);
            // transform.Rotate(Vector3.up, rotation);
            transform.Translate(Vector3.forward * (Time.deltaTime * speed));
            // var distanceSticky = transform.position - point.position;
            // Debug.Log(distanceSticky);
            // _rigidbody.MovePosition(transform.forward + distanceSticky);
        }
    }

    public void StartAnimation()
    {
        started = true;
    }
}
