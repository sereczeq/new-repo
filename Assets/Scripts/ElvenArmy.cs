﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ElvenArmy : MonoBehaviour
{
    public int num = 34;

    public int radius = 10;

    public Transform point;
    // Start is called before the first frame update
    void Start()
    {
        var soldiers = new List<Transform>();
        foreach (Transform child in transform)
        {
            if (child.CompareTag("Elf"))
            {
                soldiers.Add(child);
            }
        }

    Debug.Log(soldiers.Count);
        for (int i = 0; i < soldiers.Count; i++){
         
            /* Distance around the circle */  
            var radians = 2 * Mathf.PI / num * i;
         
            /* Get the vector direction */ 
            var horizontal = Mathf.Cos(radians); 
            var vertical = Mathf.Sin(radians);
         
            var spawnDir = new Vector3 (horizontal, 0, vertical);
         
            /* Get the spawn position */ 
            var spawnPos = point.position + spawnDir * radius; // Radius is just the distanceSticky away from the point

            var enemy = soldiers[i];
            /* Rotate the enemy to face towards player */
            enemy.transform.LookAt(point);
            // enemy.transform.Translate(spawnPos);
            // /* Adjust height */
            // enemy.transform.Translate (new Vector3 (0, enemy.transform.localScale.y / 2, 0));
            Debug.Log(i);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
