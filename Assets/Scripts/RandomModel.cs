﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomModel : MonoBehaviour
{
    public List<GameObject> models;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var VARIABLE in models)
        {
            VARIABLE.SetActive(false);
        }
        models[Random.Range(0, models.Count)].SetActive(true);
    }
}
