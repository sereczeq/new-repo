﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BuildingCanvas : MonoBehaviour
{
    public GameObject narratorOne;
    public GameObject narratorTwo;
    public GameObject fightButton;
    public GameObject roads;
    public GameObject houses;
    public GameObject factories;
    public GameObject blackout;
    public void FightOnClick()
    {
        SceneManager.LoadScene(2);
        AudioManager.instance.PlayBattlefield();
    }

    public void OKOnClick()
    {
        narratorOne.SetActive(false);
        blackout.SetActive(false);
    }

    public void RoadsOnClick()
    {
        Debug.Log("CLicked");
        roads.SetActive(true);
        houses.SetActive(false);
        factories.SetActive(false);
    }

    public void HousesOnClick()
    {
        roads.SetActive(false);
        houses.SetActive(true);
        factories.SetActive(false);
    }

    public void FactoriesOnClick()
    {
        roads.SetActive(false);
        houses.SetActive(false);
        factories.SetActive(true);
    }

    public void ShowNarratorTwo()
    {
        narratorTwo.SetActive(true);
        fightButton.SetActive(true);
    }
    
}
