﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FightButton : MonoBehaviour
{
    public void OnClick()
    {
        SceneManager.LoadScene(2);
        AudioManager.instance.PlayBattlefield();
    }
}
