﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResourcesAnimator : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        foreach (var VARIABLE in GameObject.FindGameObjectsWithTag("Elf"))
        {
            VARIABLE.GetComponentInChildren<Animator>().SetTrigger("Run");
        }
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(4);
    }
}
